variable "enterprise_license" {}
variable "ingress_annotations" { default = {} }
variable "ingress_class" { default = "kong" }
variable "k8s_ca_data" {}
variable "k8s_server" {}
variable "k8s_token" {}
variable "kong_admin_access_log" { default = "/dev/stdout" }
variable "kong_admin_error_log" { default = "/dev/stderr" }
variable "kong_admin_gui_auth" { default = "basic-auth" }
variable "kong_admin_host" {}
variable "kong_admin_listen" { default = "0.0.0.0:8001, 0.0.0.0:8444 ssl" }
variable "kong_api_key" {}
variable "kong_enforce_rbac" { default = "on" }
variable "kong_enterprise_superuser_password" { default = "cloudnative" }
variable "kong_image" { default = "kong-docker-kong-enterprise-edition-docker.bintray.io/kong-enterprise-edition:1.3.0.1-alpine" }
variable "kong_ingress_image" { default = "kong-docker-kubernetes-ingress-controller.bintray.io/kong-ingress-controller:0.7.1" }
variable "kong_manager_host" {}
variable "kong_nginx_http_include" { default = "/kong/servers.conf" }
variable "kong_nginx_worker_processes" { default = "1" }
variable "kong_pg_database" { default = "kong" }
variable "kong_pg_host" { default = "postgres" }
variable "kong_pg_password" { default = "kong" }
variable "kong_pg_port" { default = "5432" }
variable "kong_pg_user" { default = "kong" }
variable "kong_portal" { default = "on" }
variable "kong_portal_api_access_log" { default = "/dev/stdout" }
variable "kong_portal_api_error_log" { default = "/dev/stderr" }
variable "kong_portal_api_listen" { default = "0.0.0.0:8004" }
variable "kong_portal_auth" { default = "basic-auth" }
variable "kong_portal_gui_listen" { default = "0.0.0.0:8003" }
variable "kong_portal_gui_protocol" { default = "http" }
variable "kong_portal_host" {}
variable "kong_portalapi_host" {}
variable "kong_proxy_listen" { default = "0.0.0.0:8000, 0.0.0.0:8443 ssl http2" }
variable "kong_username" {}
variable "namespace" { default = "kong" }
variable "on_aws" { default = false }
variable "proxy_service_annotations" { default = {} }
variable "service_annotations" { default = {} }
