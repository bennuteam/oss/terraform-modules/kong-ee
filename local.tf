locals {
  docker_secret = {
    "docker_username" = var.kong_username
    "docker_password" = var.kong_api_key
    "docker_auth"     = base64encode(format("%s:%s", var.kong_username, var.kong_api_key))
  }

  ingress_annotations       = merge({ "kubernetes.io/ingress.class" = var.ingress_class }, var.ingress_annotations)
  kong_database             = "postgres"
  kong_portal_api_url       = format("http://%s", var.kong_portalapi_host)
  proxy_service_annotations = var.on_aws ? merge({ "service.beta.kubernetes.io/aws-load-balancer-backend-protocol" = "tcp", "service.beta.kubernetes.io/aws-load-balancer-type" = "nlb" }, var.proxy_service_annotations) : var.proxy_service_annotations
  selector_labels           = { "app" = "ingress-kong" }
  service_annotations       = var.on_aws ? merge({ "service.beta.kubernetes.io/aws-load-balancer-proxy-protocol" = "*" }, var.service_annotations) : var.service_annotations

  webhook_ca_keyusage = [
    "any_extended",
    "cert_signing",
    "client_auth",
    "code_signing",
    "content_commitment", # nonRepudiation
    "crl_signing",
    "data_encipherment",
    "decipher_only",
    "digital_signature",
    "email_protection",
    "encipher_only",
    "ipsec_end_system",
    "ipsec_tunnel",
    "ipsec_user",
    "key_agreement",
    "key_encipherment",
    "netscape_server_gated_crypto",
    "ocsp_signing",
    "server_auth",
    "timestamping",
  ]
}
