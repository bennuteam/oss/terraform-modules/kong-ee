apiVersion: v1
kind: Config
clusters:
- cluster:
    certificate-authority-data: ${ca_data}
    server: ${server}
  name: aws
contexts:
- context:
    cluster: aws
    user: aws
  name: aws
current-context: aws
users:
  - name: aws
    user:
      token: ${token}
