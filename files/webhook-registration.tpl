kind: ValidatingWebhookConfiguration
apiVersion: admissionregistration.k8s.io/v1beta1
metadata:
  name: kong-validations
webhooks:
- name: validations.kong.konghq.com
  failurePolicy: Fail
  sideEffects: None
  admissionReviewVersions: ["v1beta1"]
  rules:
  - apiGroups:
    - configuration.konghq.com
    apiVersions:
    - '*'
    operations:
    - CREATE
    - UPDATE
    resources:
    - kongconsumers
    - kongplugins
  clientConfig:
    caBundle: ${ca_cert}
    service:
      name: ${service}
      namespace: ${namespace}
