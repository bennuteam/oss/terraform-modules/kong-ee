# Default values for Kong's Helm Chart.
# Declare variables to be passed into your templates.
#
# Sections:
# - Kong parameters
# - Ingress Controller parameters
# - Postgres sub-chart parameters
# - Miscellaneous parameters
# - Kong Enterprise parameters

# -----------------------------------------------------------------------------
# Kong parameters
# -----------------------------------------------------------------------------

# Specify Kong configurations
# Kong configurations guide https://docs.konghq.com/latest/configuration
# Values here take precedence over values from other sections of values.yaml,
# e.g. setting pg_user here will override the value normally set when postgresql.enabled
# is set below. In general, you should not set values here if they are set elsewhere.
env:
%{ if env_database != "off" }
  database: "${env_database}"
%{ endif ~}
  nginx_worker_processes: "${env_nginx_worker_processes}"
  proxy_access_log: ${env_proxy_access_log}
  admin_access_log: ${env_admin_access_log}
  admin_gui_access_log: ${env_admin_gui_access_log}
  portal_api_access_log: ${env_portal_api_access_log}
  proxy_error_log: ${env_proxy_error_log}
  admin_error_log: ${env_admin_error_log}
  admin_gui_error_log: ${env_admin_gui_error_log}
  portal_api_error_log: ${env_portal_api_error_log}
  prefix: ${env_prefix}

# Specify Kong's Docker image and repository details here
image:
  repository: ${image_repository}
  # repository: kong-docker-kong-enterprise-k8s.bintray.io/kong-enterprise-k8s
  # repository: kong-docker-kong-enterprise-edition-docker.bintray.io/kong-enterprise-edition
  tag: "${image_tag}"
  pullPolicy: ${image_pull_policy}
  ## Optionally specify an array of imagePullSecrets.
  ## Secrets must be manually created in the namespace.
  ## If using the official Kong Enterprise registry above, you MUST provide a secret.
  ## ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
  ##
%{ if image_pull_secrets != [] ~}
  pullSecrets:
%{ for secret in image_pull_secrets ~}
    - ${secret}
%{ endfor ~}
%{ endif ~}

# Specify Kong admin service configuration
# Note: It is recommended to not use the Admin API to configure Kong
# when using Kong as an Ingress Controller.

admin:
  enabled: ${admin_enabled}
  # If you want to specify annotations for the admin service, uncomment the following
  # line, add additional or adjust as needed, and remove the curly braces after 'annotations:'.
%{ if admin_annotations != null }
  annotations:
%{ for k, v in admin_annotations ~}
    "${k}": "${v}"
%{ endfor ~}
%{ endif ~}
  # HTTP plai
  # HTTPS traffic on the admin port
  # if set to false also set readinessProbe and livenessProbe httpGet scheme's to 'HTTP'
  useTLS: ${admin_use_tls}
  servicePort: ${admin_service_port}
  containerPort: ${admin_container_port}
  # Kong admin service type
  type: ${admin_type}
  # Set a nodePort which is available
  # nodePort: 32444
  # Kong admin ingress settings. Useful if you want to expose the Admin
  # API of Kong outside the k8s cluster.
  ingress:
    # Enable/disable exposure using ingress.
    enabled: ${admin_ingress_enabled}
    # TLS secret name.
    # tls: kong-admin.example.com-tls
    # Ingress hostname
    hostname: ${admin_ingress_hostname}
    # Map of ingress annotations.
%{ if admin_ingress_annotations != null }
    annotations:
%{ for k, v in admin_ingress_annotations ~}
      "${k}": "${v}"
%{ endfor ~}
%{ endif ~}
    # Ingress path.
    path: ${admin_ingress_path}

# Specify Kong proxy service configuration
proxy:
  # If you want to specify annotations for the proxy service, uncomment the following
  # line, add additional or adjust as needed, and remove the curly braces after 'annotations:'.
%{ if proxy_annotations != null }
  annotations:
%{ for k, v in proxy_annotations ~}
    "${k}": "${v}"
%{ endfor ~}
%{ endif ~}

  # HTTP plain-text traffic
  http:
    enabled: ${proxy_http_enabled}
    servicePort: ${proxy_http_service_port}
    containerPort: ${proxy_http_container_port}
    # Set a nodePort which is available if service type is NodePort
    # nodePort: 32080

  tls:
    enabled: ${proxy_tls_enabled}
    servicePort: ${proxy_tls_service_port}
    containerPort: ${proxy_tls_container_port}
    # Set a target port for the TLS port in proxy service, useful when using TLS
    # termination on an ELB.
    # overrideServiceTargetPort: 8000
    # Set a nodePort which is available if service type is NodePort
    # nodePort: 32443

  type: ${proxy_type}

  # Kong proxy ingress settings.
  # Note: You need this only if you are using another Ingress Controller
  # to expose Kong outside the k8s cluster.
  ingress:
    # Enable/disable exposure using ingress.
    enabled: ${proxy_ingress_enabled}
%{ if proxy_ingress_hosts != [] ~}
    hosts:
%{ for host in proxy_ingress_hosts ~}
      - ${host}
%{ endfor ~}
%{ endif ~}
    # TLS section. Unlike other ingresses, this follows the format at
    # https://kubernetes.io/docs/concepts/services-networking/ingress/#tls
    # tls:
    # - hosts:
    #   - 1.example.com
    #   secretName: example1-com-tls-secret
    # - hosts:
    #   - 2.example.net
    #   secretName: example2-net-tls-secret
    # Map of ingress annotations.
%{ if proxy_ingress_annotations != null }
    annotations:
%{ for k, v in proxy_ingress_annotations ~}
      "${k}": "${v}"
%{ endfor ~}
%{ endif ~}
    # Ingress path.
    path: ${proxy_ingress_path}

%{ if proxy_external_ips != [] ~}
  externalIPs: 
%{ for ip in proxy_external_ips ~}
    - ${ip}
%{ endfor ~}
%{ endif ~}

# Custom Kong plugins can be loaded into Kong by mounting the plugin code
# into the file-system of Kong container.
# The plugin code should be present in ConfigMap or Secret inside the same
# namespace as Kong is being installed.
# The `name` property refers to the name of the ConfigMap or Secret
# itself, while the pluginName refers to the name of the plugin as it appears
# in Kong.
# Subdirectories (which are optional) require separate ConfigMaps/Secrets.
# "path" indicates their directory under the main plugin directory: the example
# below will mount the contents of kong-plugin-rewriter-migrations at "/opt/kong/rewriter/migrations".
plugins: {}
  # configMaps:
  # - pluginName: rewriter
  #   name: kong-plugin-rewriter
  #   subdirectories:
  #   - name: kong-plugin-rewriter-migrations
  #     path: migrations
  # secrets:
  # - pluginName: rewriter
  #   name: kong-plugin-rewriter
# Inject specified secrets as a volume in Kong Container at path /etc/secrets/{secret-name}/
# This can be used to override default SSL certificates.
# Be aware that the secret name will be used verbatim, and that certain types
# of punctuation (e.g. `.`) can cause issues.
# Example configuration
# secretVolumes:
# - kong-proxy-tls
# - kong-admin-tls
secretVolumes: []

# Set runMigrations to run Kong migrations
runMigrations: ${run_migrations}

# Kong's configuration for DB-less mode
# Note: Use this section only if you are deploying Kong in DB-less mode
# and not as an Ingress Controller.
dblessConfig:
  # Either Kong's configuration is managed from an existing ConfigMap (with Key: kong.yml)
  configMap: ""
  # Or the configuration is passed in full-text below
  config:
    _format_version: "1.1"
    services:
      # Example configuration
      # - name: example.com
      #   url: http://example.com
      #   routes:
      #   - name: example
      #     paths:
      #     - "/example"

# -----------------------------------------------------------------------------
# Ingress Controller parameters
# -----------------------------------------------------------------------------

# Kong Ingress Controller's primary purpose is to satisfy Ingress resources
# created in k8s.  It uses CRDs for more fine grained control over routing and
# for Kong specific configuration.
ingressController:
  enabled: ${ingress_controller_enabled}
  image:
    repository: ${ingress_controller_image_repository}
    tag: "${ingress_controller_image_tag}"

  # Specify Kong Ingress Controller configuration via environment variables
  env:
    ${ingress_controller_env}

  admissionWebhook:
    enabled: ${ingress_controller_admission_webhook_enabled}
    failurePolicy: ${ingress_controller_admission_webhook_failure_policy}
    port: ${ingress_controller_admission_webhook_port}

  ingressClass: ${ingress_controller_ingress_class}

  rbac:
    # Specifies whether RBAC resources should be created
    create: ${ingress_controller_rbac_create}

  serviceAccount:
    # Specifies whether a ServiceAccount should be created
    create: ${ingress_controller_service_account_create}
    # The name of the ServiceAccount to use.
    # If not set and create is true, a name is generated using the fullname template
    name: ${ingress_controller_service_account_name}

  installCRDs: ${ingress_controller_install_crds}

  # general properties
  livenessProbe:
    httpGet:
      path: "/healthz"
      port: 10254
      scheme: HTTP
    initialDelaySeconds: 5
    timeoutSeconds: 5
    periodSeconds: 10
    successThreshold: 1
    failureThreshold: 3
  readinessProbe:
    httpGet:
      path: "/healthz"
      port: 10254
      scheme: HTTP
    initialDelaySeconds: 5
    timeoutSeconds: 5
    periodSeconds: 10
    successThreshold: 1
    failureThreshold: 3
  resources: {}

# -----------------------------------------------------------------------------
# Postgres sub-chart parameters
# -----------------------------------------------------------------------------

# Kong can run without a database or use either Postgres or Cassandra
# as a backend datatstore for it's configuration.
# By default, this chart installs Kong without a database.

# If you would like to use a database, there are two options:
# - (recommended) Deploy and maintain a database and pass the connection
#   details to Kong via the `env` section.
# - You can use the below `postgresql` sub-chart to deploy a database
#   along-with Kong as part of a single Helm release.

# PostgreSQL chart documentation:
# https://github.com/helm/charts/blob/master/stable/postgresql/README.md

postgresql:
  enabled: ${postgresql_enabled}
  # postgresqlUsername: ${postgresql_postgresql_username}
  # postgresqlDatabase: ${postgresql_postgresql_database}
  # service:
  #   port: ${postgresql_service_port}

# -----------------------------------------------------------------------------
# Miscellaneous parameters
# -----------------------------------------------------------------------------

waitImage:
  repository: ${wait_image_repository}
  tag: ${wait_image_tag}
  pullPolicy: ${wait_image_pull_policy}

# update strategy
updateStrategy:
  type: ${update_estrategy_type}
  # rollingUpdate:
  #   maxSurge: "100%"
  #   maxUnavailable: "0%"

# If you want to specify resources, uncomment the following
# lines, adjust them as necessary, and remove the curly braces after 'resources:'.
resources:
  limits:
   cpu: ${resources_limits_cpu}
   memory: ${resources_limits_memory}
  requests:
   cpu: ${resources_request_cpu}
   memory: ${resources_request_memory}

# readinessProbe for Kong pods
# If using Kong Enterprise with RBAC, you must add a Kong-Admin-Token header
readinessProbe:
  httpGet:
    path: "/status"
    port: metrics
    scheme: HTTP
  initialDelaySeconds: 5
  timeoutSeconds: 5
  periodSeconds: 10
  successThreshold: 1
  failureThreshold: 3

# livenessProbe for Kong pods
livenessProbe:
  httpGet:
    path: "/status"
    port: metrics
    scheme: HTTP
  initialDelaySeconds: 5
  timeoutSeconds: 5
  periodSeconds: 10
  successThreshold: 1
  failureThreshold: 3

# Affinity for pod assignment
# Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity
# affinity: {}

# Tolerations for pod assignment
# Ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
tolerations: []

# Node labels for pod assignment
# Ref: https://kubernetes.io/docs/user-guide/node-selection/
nodeSelector: {}

# Annotation to be added to Kong pods
podAnnotations: {}

# Kong pod count
replicaCount: 1

# Enable autoscaling using HorizontalPodAutoscaler
autoscaling:
  enabled: ${autoscaling_enabled}
  minReplicas: ${autoscaling_min_replicas}
  maxReplicas: ${autoscaling_max_replicas}
  ## targetCPUUtilizationPercentage only used if the cluster doesn't support autoscaling/v2beta
  targetCPUUtilizationPercentage:
  ## Otherwise for clusters that do support autoscaling/v2beta, use metrics
  metrics:
    - type: Resource
      resource:
        name: cpu
        target:
          type: Utilization
          averageUtilization: 80

# Kong Pod Disruption Budget
podDisruptionBudget:
  enabled: ${pod_disruption_budget_enabled}
  maxUnavailable: "${pod_disruption_budget_max_unavailable}"

podSecurityPolicy:
  enabled: ${pod_security_policy_enabled}

priorityClassName: ${priority_class_name}

# securityContext for Kong pods.
securityContext:
  runAsUser: 1000

serviceMonitor:
  # Specifies whether ServiceMonitor for Prometheus operator should be created
  enabled: ${service_monitor_enabled}
  # interval: 10s
  # Specifies namespace, where ServiceMonitor should be installed
  namespace: ${service_monitor_namespace}
  # labels:
  #   foo: bar

# -----------------------------------------------------------------------------
# Kong Enterprise parameters
# -----------------------------------------------------------------------------

# Toggle Kong Enterprise features on or off
# RBAC and SMTP configuration have additional options that must all be set together
# Other settings should be added to the "env" settings below
enterprise:
  enabled: ${enterprise_enabled}
  # Kong Enterprise license secret name
  # This secret must contain a single 'license' key, containing your base64-encoded license data
  # The license secret is required for all Kong Enterprise deployments
  license_secret: ${enterprise_license_secret}
  vitals:
    enabled: ${enterprise_vital_enabled}
  portal:
    enabled: ${enterprise_portal_enabled}
  rbac:
    enabled: ${enterprise_rbac_enabled}
    admin_gui_auth: ${enterprise_rbac_admin_gui_auth}
    # If RBAC is enabled, this Secret must contain an admin_gui_session_conf key
    # The key value must be a secret configuration, following the example at
    # https://docs.konghq.com/enterprise/latest/kong-manager/authentication/sessions
    session_conf_secret: ${enterprise_rbac_session_conf_secret}
    # If admin_gui_auth is not set to basic-auth, provide a secret name which
    # has an admin_gui_auth_conf key containing the plugin config JSON
    admin_gui_auth_conf_secret: ${enterprise_rbac_admin_gui_auth_conf_secret}
  # For configuring emails and SMTP, please read through:
  # https://docs.konghq.com/enterprise/latest/developer-portal/configuration/smtp
  # https://docs.konghq.com/enterprise/latest/kong-manager/networking/email
  smtp:
    enabled: ${enterprise_smtp_enabled}
    portal_emails_from: ${enterprise_smtp_portal_emails_from}
    portal_emails_reply_to: ${enterprise_smtp_portal_emails_reply_to}
    admin_emails_from: ${enterprise_smtp_admin_emails_from}
    admin_emails_reply_to: ${enterprise_smtp_admin_emails_reply_to}
    smtp_admin_emails: ${enterprise_smtp_smtp_admin_emails}
    smtp_host: ${enterprise_smtp_smtp_host}
    smtp_port: ${enterprise_smtp_smtp_port}
    smtp_starttls: ${enterprise_smtp_smtp_starttls}
    auth:
      # If your SMTP server does not require authentication, this section can
      # be left as-is. If smtp_username is set to anything other than an empty
      # string, you must create a Secret with an smtp_password key containing
      # your SMTP password and specify its name here.
      smtp_username: ${enterprise_smtp_auth_smtp_username}  # e.g. postmaster@example.com
      smtp_password_secret: ${enterprise_smtp_smtp_password_secret}

manager:
  # If you want to specify annotations for the Manager service, uncomment the following
  # line, add additional or adjust as needed, and remove the curly braces after 'annotations:'.
%{ if manager_annotations != null }
  annotations:
%{ for k, v in manager_annotations ~}
    "${k}": "${v}"
%{ endfor ~}
%{ endif ~}

  # HTTP plain-text traffic
  http:
    enabled: ${manager_http_enabled}
    servicePort: ${manager_http_service_port}
    containerPort: ${manager_http_container_port}
    # Set a nodePort which is available if service type is NodePort
    # nodePort: 32080

  tls:
    enabled: ${manager_tls_enabled}
    servicePort: ${manager_tls_service_port}
    containerPort: ${manager_tls_container_port}
    # Set a nodePort which is available if service type is NodePort
    # nodePort: 32443

  type: ${manager_type}

  # Kong proxy ingress settings.
  ingress:
    # Enable/disable exposure using ingress.
    enabled: ${manager_ingress_enabled}
    # TLS secret name.
    # tls: kong-proxy.example.com-tls
    # Ingress hostname
    hostname: ${manager_ingress_hostname}
    # Map of ingress annotations.
%{ if manager_ingress_annotations != null }
    annotations:
%{ for k, v in manager_ingress_annotations ~}
      "${k}": "${v}"
%{ endfor ~}
%{ endif ~}
    # Ingress path.
    path: ${manager_ingress_path}

%{ if manager_external_ips != [] ~}
  externalIPs: 
%{ for ip in manager_external_ips ~}
    - ${ip}
%{ endfor ~}
%{ endif ~}

portal:
  # If you want to specify annotations for the Portal service, uncomment the following
  # line, add additional or adjust as needed, and remove the curly braces after 'annotations:'.
%{ if portal_annotations != null }
  annotations:
%{ for k, v in portal_annotations ~}
    "${k}": "${v}"
%{ endfor ~}
%{ endif ~}

  # HTTP plain-text traffic
  http:
    enabled: ${portal_http_enabled}
    servicePort: ${portal_http_service_port}
    containerPort: ${portal_http_container_port}
    # Set a nodePort which is available if service type is NodePort
    # nodePort: 32080

  tls:
    enabled: ${portal_tls_enabled}
    servicePort: ${portal_tls_service_port}
    containerPort: ${portal_tls_container_port}
    # Set a nodePort which is available if service type is NodePort
    # nodePort: 32443

  type: ${portal_type}

  # Kong proxy ingress settings.
  ingress:
    # Enable/disable exposure using ingress.
    enabled: ${portal_ingress_enabled}
    # TLS secret name.
    # tls: kong-proxy.example.com-tls
    # Ingress hostname
    hostname: ${portal_ingress_hostname}
    # Map of ingress annotations.
%{ if portal_ingress_annotations != null }
    annotations:
%{ for k, v in portal_ingress_annotations ~}
      "${k}": "${v}"
%{ endfor ~}
%{ endif ~}
    # Ingress path.
    path: ${portal_ingress_path}

# %{ if portal_external_ips != [] ~}
#   externalIPs: 
# %{ for ip in portal_external_ips ~}
#     - ${ip}
# %{ endfor ~}
# %{ endif ~}

portalapi:
  # If you want to specify annotations for the Portal API service, uncomment the following
  # line, add additional or adjust as needed, and remove the curly braces after 'annotations:'.
%{ if portal_api_annotations != null }
  annotations:
%{ for k, v in portal_api_annotations ~}
    "${k}": "${v}"
%{ endfor ~}
%{ endif ~}
  # HTTP plain-text traffic
  http:
    enabled: ${portal_api_http_enabled}
    servicePort: ${portal_api_http_service_port}
    containerPort: ${portal_api_http_container_port}
    # Set a nodePort which is available if service type is NodePort
    # nodePort: 32080

  tls:
    enabled: ${portal_api_tls_enabled}
    servicePort: ${portal_api_tls_service_port}
    containerPort: ${portal_api_tls_container_port}
    # Set a nodePort which is available if service type is NodePort
    # nodePort: 32443

  type: ${portal_api_type}

  # Kong proxy ingress settings.
  ingress:
    # Enable/disable exposure using ingress.
    enabled: ${portal_api_ingress_enabled}
    # TLS secret name.
    # tls: kong-proxy.example.com-tls
    # Ingress hostname
    hostname: ${portal_api_ingress_hostname}
    # Map of ingress annotations.
%{ if portal_api_ingress_annotations != null }
    annotations:
%{ for k, v in portal_api_ingress_annotations ~}
      "${k}": "${v}"
%{ endfor ~}
%{ endif ~}
    # Ingress path.
    path: ${portal_api_ingress_path}

%{ if portal_api_external_ips != [] ~}
  externalIPs: 
%{ for ip in portal_api_external_ips ~}
    - ${ip}
%{ endfor ~}
%{ endif ~}
