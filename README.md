# Kong EE

> Kong (EE + ingress controller) for Kubernetes

This module deploys kong and postgres

## Usage

```hcl
module "kong" {
  source              = "git::https://gitlab.com/bennuteam/oss/terraform-modules/kong-ee.git?ref=4.0"
  ca_data             = var.kubernetes_ca_data
  enterprise_license  = var.enterprise_license
  kong_admin_host     = var.kong_admin_host
  kong_api_key        = var.kong_api_key
  kong_manager_host   = var.kong_manager_host
  kong_portal_host    = var.kong_portal_host
  kong_portalapi_host = var.kong_portalapi_host
  kong_username       = var.kong_username
  server              = var.kubernetes_server
  token               = var.kubernetes_token
}
```

## Customization

|Variable|Description|Required|Default|
|:---|---|:---:|---|
|`enterprise_license`|Kong enterprise license content (e.g. `file(./license.json)`)|X||
|`ingress_annotations`|Annotations for to-be-created ingress rules||`{}`|
|`ingress_class`|Ingress class name for kong ingress controller, this must be added to the ingress rules in order to use kong ingress controller (e.g. `kubernetes.io/ingress.class: kong`).<br>NOTE: The ingress resources created by this module already add this annotation.||`kong`|
|`k8s_ca_data`|kubernetes base64 encoded ca cert for kubeconfig|X||
|`k8s_server`|kubernetes server endpoint for kubeconfig|X||
|`k8s_token`|Kubernetes base64 encoded token for kubeconfig|X||
|`kong_admin_access_log`|Where should admin access logs be stored to||`/dev/stdout`|
|`kong_admin_error_log`|Where should admin error logs be stored to||`/dev/stderr`|
|`kong_admin_gui_auth`|Auth method to use in admin GUI||`basic-auth`|
|`kong_admin_host`|Host in fqdn format for kong admin api (e.g. `kong-admin.mydomain.com`)|X||
|`kong_admin_listen`|Ports to open for kong admin api||`0.0.0.0:8001, 0.0.0.0:8444 ssl`|
|`kong_api_key`|Kong bintray api key to pull images|X||
|`kong_enforce_rbac`|Enabled RBAC for kong||`on`|
|`kong_enterprise_superuser_password`|Password for kong RBAC default user `kong_admin`. It's recommened to change this value for something more secure.||`cloudnative`|
|`kong_image`|Kong EE image||`kong-docker-kong-enterprise-edition-docker.bintray.io/kong-enterprise-edition:1.3.0.1-alpine`|
|`kong_ingress_image`|Kong ingress controller image||`kong-docker-kubernetes-ingress-controller.bintray.io/kong-ingress-controller:0.7.1`|
|`kong_manager_host`|Host in fqdn format for kong admin GUI (e.g. `kong-manager.mydomain.com`)|X||
|`kong_nginx_worker_processes`|Nginx worker process to spawn for kong||`"1"`|
|`kong_pg_database`|Postgres database name to create for Kong||`kong`|
|`kong_pg_host`|Service name for the database||`kong`|
|`kong_pg_password`|Password for the database user||`kong`|
|`kong_portal_api_access_log`|Where should devportal api access logs be stored to||`/dev/stdout`|
|`kong_portal_api_error_log`|Where should devportal api error logs be stored to||`/dev/stderr`|
|`kong_portal_api_listen`|Ports to open for devportal api ||`0.0.0.0:8004`|
|`kong_portal_auth`|Dev portal auth method to use||`basic-auth`|
|`kong_portal_gui_listen`|Ports to open for Dev portal GUI||`0.0.0.0:8003`|
|`kong_portal_gui_protocol`|Scheme for Dev portal GUI||`http`|
|`kong_portal_host`|Host in fqdn format for kong Dev portal GUI (e.g. `kong-portal.mydomain.com`)|X||
|`kong_portal`|Enable kong dev portal||`on`|
|`kong_portalapi_host`|Host in fqdn format for kong Dev portal api (e.g. `kong-portalapi.mydomain.com`)|X||
|`kong_proxy_listen`|Ports to open for Kong proxy. This is where Kong handles all the traffic.||`0.0.0.0:8000, 0.0.0.0:8443 ssl http2`|
|`kong_username`|Kong bintray username to pull images|X||
|`namespace`|Namespace name to create for all resources||`kong`|
|`on_aws`|Set true if the resources are to be deployed on AWS||`false`|
|`proxy_service_annotations`|Annotations for kong proxy service in kubernetes||`{}`|
|`service_annotations`|Annotations for kubernetes services||`{}`|
