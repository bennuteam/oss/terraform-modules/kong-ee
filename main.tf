resource "tls_private_key" "ca_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "tls_self_signed_cert" "ca_pem" {
  allowed_uses          = local.webhook_ca_keyusage
  is_ca_certificate     = true
  key_algorithm         = tls_private_key.ca_key.algorithm
  private_key_pem       = tls_private_key.ca_key.private_key_pem
  validity_period_hours = 87600

  subject {
    common_name = "kong-admission-ca"
  }
}

resource "tls_private_key" "webhook_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "tls_cert_request" "webhook_csr" {
  dns_names       = [format("kong-validation-webhook.%s.svc", var.namespace)]
  key_algorithm   = tls_private_key.webhook_key.algorithm
  private_key_pem = tls_private_key.webhook_key.private_key_pem

  subject {
    common_name = format("kong-validation-webhook.%s.svc", var.namespace)
  }
}

resource "tls_locally_signed_cert" "webhook_cert" {
  allowed_uses          = local.webhook_ca_keyusage
  ca_cert_pem           = tls_self_signed_cert.ca_pem.cert_pem
  ca_key_algorithm      = tls_private_key.ca_key.algorithm
  ca_private_key_pem    = tls_private_key.ca_key.private_key_pem
  cert_request_pem      = tls_cert_request.webhook_csr.cert_request_pem
  validity_period_hours = 87600
}

resource "kubernetes_namespace" "kong" {
  metadata {
    name = var.namespace
  }
}

resource "kubernetes_secret" "validation_webhook_keypair" {
  metadata {
    name      = "validation-webhook-keypair"
    namespace = kubernetes_namespace.kong.metadata.0.name
  }

  type = "tls"
  data = {
    "tls.crt" = tls_locally_signed_cert.webhook_cert.cert_pem
    "tls.key" = tls_private_key.webhook_key.private_key_pem
  }
}

resource "kubernetes_secret" "kong_enterprise_license" {
  metadata {
    name      = "kong-enterprise-license"
    namespace = kubernetes_namespace.kong.metadata.0.name
  }

  data = {
    "license" = var.enterprise_license
  }
}

resource "kubernetes_secret" "kong_enterprise_edition_docker" {
  metadata {
    name      = "kong-enterprise-edition-docker"
    namespace = kubernetes_namespace.kong.metadata.0.name
  }

  data = {
    ".dockerconfigjson" = templatefile(format("%s/files/docker.tpl", path.module), merge({ "docker_server" = "kong-docker-kong-enterprise-edition-docker.bintray.io" }, local.docker_secret))
  }

  type = "kubernetes.io/dockerconfigjson"
}

resource "kubernetes_secret" "kong_enterprise_superuser_password" {
  metadata {
    name      = "kong-enterprise-superuser-password"
    namespace = kubernetes_namespace.kong.metadata.0.name
  }

  data = {
    "password" = var.kong_enterprise_superuser_password
  }
}

resource "local_file" "kube_config" {
  content  = templatefile(format("%s/files/kubeconfig.tpl", path.module), { "ca_data" = var.k8s_ca_data, "server" = var.k8s_server, "token" = var.k8s_token })
  filename = format("%s/kubeconfig", path.module)
}

resource "null_resource" "kong_crds" {
  depends_on = [local_file.kube_config, kubernetes_namespace.kong]

  provisioner "local-exec" {
    command = "kubectl --kubeconfig=${local_file.kube_config.filename} apply -f=${format("%s/files/crds.yml", path.module)}"
  }

  # provisioner "local-exec" {
  #   when    = destroy
  #   command = "kubectl --kubeconfig=${local_file.kube_config.filename} delete -f=${format("%s/files/crds.yml", path.module)}"
  # }
}

resource "local_file" "kong_validations" {
  content  = templatefile(format("%s/files/webhook-registration.tpl", path.module), { "ca_cert" = base64encode(tls_self_signed_cert.ca_pem.cert_pem), "service" = kubernetes_service.kong_validation_webhook.metadata.0.name, "namespace" = kubernetes_namespace.kong.metadata.0.name })
  filename = format("%s/webhook-registration.yml", path.module)
}

resource "null_resource" "kong_validations" {
  depends_on = [local_file.kube_config, local_file.kong_validations, kubernetes_namespace.kong]

  provisioner "local-exec" {
    command = "kubectl --kubeconfig=${local_file.kube_config.filename} apply -f=${local_file.kong_validations.filename}"
  }

  # provisioner "local-exec" {
  #   when    = destroy
  #   command = "kubectl --kubeconfig=${local_file.kube_config.filename} delete -f=${local_file.kong_validations.filename}"
  # }
}

resource "kubernetes_service_account" "kong_serviceaccount" {
  metadata {
    name      = "kong-serviceaccount"
    namespace = kubernetes_namespace.kong.metadata.0.name
  }

  automount_service_account_token = true
}

resource "kubernetes_cluster_role" "kong_ingress_clusterrole" {
  metadata {
    name = "kong-ingress-clusterrole"
  }

  rule {
    api_groups = [""]
    resources  = ["endpoints", "nodes", "pods", "secrets"]
    verbs      = ["list", "watch"]
  }

  rule {
    api_groups = [""]
    resources  = ["nodes"]
    verbs      = ["get"]
  }

  rule {
    api_groups = [""]
    resources  = ["services"]
    verbs      = ["get", "list", "watch"]
  }
  rule {
    api_groups = ["networking.k8s.io", "extensions"]
    resources  = ["ingresses"]
    verbs      = ["get", "list", "watch"]
  }
  rule {
    api_groups = [""]
    resources  = ["events"]
    verbs      = ["create", "patch"]
  }

  rule {
    api_groups = ["networking.k8s.io", "extensions"]
    resources  = ["ingresses/status"]
    verbs      = ["update"]
  }
  rule {
    api_groups = ["configuration.konghq.com"]
    resources  = ["kongplugins", "kongcredentials", "kongconsumers", "kongingresses"]
    verbs      = ["get", "list", "watch"]
  }
  rule {
    api_groups     = [""]
    resources      = ["configmaps"]
    resource_names = ["ingress-controller-leader-kong"]
    verbs          = ["get", "update"]
  }
  rule {
    api_groups = [""]
    resources  = ["configmaps"]
    verbs      = ["create"]
  }
}

resource "kubernetes_cluster_role_binding" "kong_ingress_clusterrole_nisa_binding" {
  metadata {
    name = "kong-ingress-clusterrole-nisa-binding"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role.kong_ingress_clusterrole.metadata.0.name
  }

  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.kong_serviceaccount.metadata.0.name
    namespace = kubernetes_service_account.kong_serviceaccount.metadata.0.namespace
  }
}

resource "kubernetes_config_map" "kong_server_blocks" {
  metadata {
    name      = "kong-server-blocks"
    namespace = kubernetes_namespace.kong.metadata.0.name
  }

  data = {
    "servers.conf" = <<EOF
# Prometheus metrics server
server {
    server_name kong_prometheus_exporter;
    listen 0.0.0.0:9542; # can be any other port as well
    access_log off;

    location /metrics {
        default_type text/plain;
        content_by_lua_block {
              local prometheus = require "kong.plugins.prometheus.exporter"
              prometheus:collect()
        }
    }

    location /nginx_status {
        internal;
        stub_status;
    }
}
# Health check server
server {
    server_name kong_health_check;
    listen 0.0.0.0:9001; # can be any other port as well

    access_log off;
    location /health {
      return 200;
    }
}
EOF
  }
}

resource "kubernetes_config_map" "kong_session_conf" {
  metadata {
    name      = "kong-session-conf"
    namespace = kubernetes_namespace.kong.metadata.0.name
  }

  data = {
    "admin-gui-session-conf" = templatefile(format("%s/files/session-conf.tpl", path.module), { "cookie_secure" = false, "cookie_name" = "admin_session", "cookie_lifetime" = 31557600, "cookie_samesite" = "off", "secret" = "please-change-me" })
    "portal-session-conf"    = templatefile(format("%s/files/session-conf.tpl", path.module), { "cookie_secure" = false, "cookie_name" = "portal_session", "cookie_lifetime" = 31557600, "cookie_samesite" = "off", "secret" = "please-change-me" })
  }
}


resource "kubernetes_service" "kong_admin" {
  metadata {
    name        = "kong-admin"
    namespace   = kubernetes_namespace.kong.metadata.0.name
    annotations = local.service_annotations
  }

  spec {
    port {
      name        = "admin"
      port        = 80
      protocol    = "TCP"
      target_port = 8001
    }

    selector = local.selector_labels

    type = "ClusterIP"
  }
}

resource "kubernetes_service" "kong_manager" {
  metadata {
    name        = "kong-manager"
    namespace   = kubernetes_namespace.kong.metadata.0.name
    annotations = local.service_annotations
  }

  spec {
    port {
      name        = "manager"
      port        = 80
      protocol    = "TCP"
      target_port = 8002
    }

    selector = local.selector_labels

    type = "ClusterIP"
  }
}

resource "kubernetes_service" "kong_proxy" {
  metadata {
    name        = "kong-proxy"
    namespace   = kubernetes_namespace.kong.metadata.0.name
    annotations = local.proxy_service_annotations
  }

  spec {
    external_traffic_policy = "Local"
    port {
      name        = "proxy"
      port        = 80
      protocol    = "TCP"
      target_port = 8000
    }

    port {
      name        = "proxy-ssl"
      port        = 443
      protocol    = "TCP"
      target_port = 8443
    }

    selector = local.selector_labels

    type = "LoadBalancer"
  }
}

resource "kubernetes_service" "kong_portalapi" {
  metadata {
    name        = "kong-portalapi"
    namespace   = kubernetes_namespace.kong.metadata.0.name
    annotations = local.service_annotations
  }

  spec {
    port {
      name        = "portalapi"
      port        = 80
      protocol    = "TCP"
      target_port = 8004
    }

    selector = local.selector_labels
    type     = "ClusterIP"
  }
}

resource "kubernetes_service" "kong_portal" {
  metadata {
    name        = "kong-portal"
    namespace   = kubernetes_namespace.kong.metadata.0.name
    annotations = local.service_annotations
  }

  spec {
    port {
      name        = "portal"
      port        = 80
      protocol    = "TCP"
      target_port = 8003
    }

    selector = local.selector_labels
    type     = "ClusterIP"
  }
}

resource "kubernetes_service" "kong_validation_webhook" {
  metadata {
    name      = "kong-validation-webhook"
    namespace = kubernetes_namespace.kong.metadata.0.name
  }

  spec {
    port {
      name        = "webhook"
      port        = 443
      protocol    = "TCP"
      target_port = 8080
    }

    selector = local.selector_labels
    type     = "ClusterIP"
  }
}

resource "kubernetes_service" "postgres" {
  metadata {
    name      = var.kong_pg_host
    namespace = kubernetes_namespace.kong.metadata.0.name
  }

  spec {
    port {
      name        = "postgres"
      port        = 5432
      protocol    = "TCP"
      target_port = 5432
    }

    selector = {
      "app" = "postgres"
    }

    type = "ClusterIP"
  }
}

resource "kubernetes_service" "fallback" {
  metadata {
    name      = "fallback"
    namespace = kubernetes_namespace.kong.metadata.0.name
    labels = {
      "app" = "fallback"
    }
  }

  spec {
    type = "ClusterIP"
    port {
      name        = "http"
      port        = 80
      protocol    = "TCP"
      target_port = 5678
    }
    selector = {
      "app" = "fallback"
    }
  }
}

resource "kubernetes_ingress" "kong_admin" {
  metadata {
    name        = "kong-admin"
    namespace   = kubernetes_namespace.kong.metadata.0.name
    annotations = local.ingress_annotations
  }

  spec {
    rule {
      host = var.kong_admin_host
      http {
        path {
          path = "/"
          backend {
            service_name = kubernetes_service.kong_admin.metadata.0.name
            service_port = 80
          }
        }
      }
    }
  }
}

resource "kubernetes_ingress" "kong_manager" {
  metadata {
    name        = "kong-manager"
    namespace   = kubernetes_namespace.kong.metadata.0.name
    annotations = local.ingress_annotations
  }

  spec {
    rule {
      host = var.kong_manager_host
      http {
        path {
          path = "/"
          backend {
            service_name = kubernetes_service.kong_manager.metadata.0.name
            service_port = 80
          }
        }
      }
    }
  }
}

resource "kubernetes_ingress" "kong_portalapi" {
  metadata {
    name        = "kong-portalapi"
    namespace   = kubernetes_namespace.kong.metadata.0.name
    annotations = local.ingress_annotations
  }

  spec {
    rule {
      host = var.kong_portalapi_host
      http {
        path {
          path = "/"
          backend {
            service_name = kubernetes_service.kong_portalapi.metadata.0.name
            service_port = 80
          }
        }
      }
    }
  }
}

resource "kubernetes_ingress" "kong_portal" {
  metadata {
    name        = "kong-portal"
    namespace   = kubernetes_namespace.kong.metadata.0.name
    annotations = local.ingress_annotations
  }

  spec {
    rule {
      host = var.kong_portal_host
      http {
        path {
          path = "/"
          backend {
            service_name = kubernetes_service.kong_portal.metadata.0.name
            service_port = 80
          }
        }
      }
    }
  }
}

resource "kubernetes_ingress" "fallback" {
  metadata {
    name      = "fallback"
    namespace = kubernetes_namespace.kong.metadata.0.name
    annotations = {
      "kubernetes.io/ingress.class" = var.ingress_class
    }
  }

  spec {
    backend {
      service_name = kubernetes_service.fallback.metadata.0.name
      service_port = 80
    }
  }
}

resource "kubernetes_deployment" "ingress-kong" {
  metadata {
    name      = "ingress-kong"
    namespace = kubernetes_namespace.kong.metadata.0.name
    labels    = local.selector_labels
  }

  spec {
    replicas = 1
    selector {
      match_labels = local.selector_labels
    }
    template {
      metadata {
        annotations = {
          "kuma.io/gateway"                              = "enabled"
          "prometheus.io/port"                           = "9542"
          "prometheus.io/scrape"                         = "true"
          "traffic.sidecar.istio.io/includeInboundPorts" = ""
        }
        labels = local.selector_labels
      }
      spec {
        service_account_name            = kubernetes_service_account.kong_serviceaccount.metadata.0.name
        automount_service_account_token = true
        image_pull_secrets {
          name = kubernetes_secret.kong_enterprise_edition_docker.metadata.0.name
        }
        init_container {
          name    = "wait-for-migrations"
          image   = var.kong_image
          command = ["/bin/sh", "-c", "while true; do kong migrations list; if [[ 0 -eq $? ]]; then exit 0; fi; sleep 2;  done;"]
          env {
            name = "KONG_LICENSE_DATA"
            value_from {
              secret_key_ref {
                name = kubernetes_secret.kong_enterprise_license.metadata.0.name
                key  = "license"
              }
            }
          }
          env {
            name  = "KONG_PG_HOST"
            value = var.kong_pg_host
          }
          env {
            name  = "KONG_PG_PASSWORD"
            value = var.kong_pg_password
          }
          env {
            name  = "KONG_PORTAL"
            value = var.kong_portal
          }
          env {
            name  = "KONG_PORTAL_API_LISTEN"
            value = var.kong_portal_api_listen
          }
          env {
            name  = "KONG_PORTAL_GUI_LISTEN"
            value = var.kong_portal_gui_listen
          }
          env {
            name  = "KONG_PORTAL_GUI_HOST"
            value = var.kong_portal_host
          }
          env {
            name  = "KONG_PORTAL_GUI_PROTOCOL"
            value = var.kong_portal_gui_protocol
          }
          env {
            name  = "KONG_PORTAL_API_URL"
            value = local.kong_portal_api_url
          }
          env {
            name  = "KONG_PORTAL_API_ACCESS_LOG"
            value = var.kong_portal_api_access_log
          }
          env {
            name  = "KONG_PORTAL_API_ERROR_LOG"
            value = var.kong_portal_api_error_log
          }
          env {
            name  = "KONG_PORTAL_AUTH"
            value = var.kong_portal_auth
          }
          env {
            name = "KONG_PORTAL_SESSION_CONF"
            value_from {
              config_map_key_ref {
                name = kubernetes_config_map.kong_session_conf.metadata.0.name
                key  = "portal-session-conf"
              }
            }
          }
        }
        container {
          name  = "proxy"
          image = var.kong_image
          port {
            container_port = 8001
            name           = "admin"
            protocol       = "TCP"
          }
          port {
            container_port = 8002
            name           = "manager"
            protocol       = "TCP"
          }
          port {
            container_port = 8000
            name           = "proxy"
            protocol       = "TCP"
          }
          port {
            container_port = 8443
            name           = "proxy-ssl"
            protocol       = "TCP"
          }
          port {
            container_port = 8003
            name           = "portalapi"
            protocol       = "TCP"
          }
          port {
            container_port = 8004
            name           = "portal"
            protocol       = "TCP"
          }
          env {
            name = "KONG_LICENSE_DATA"
            value_from {
              secret_key_ref {
                name = kubernetes_secret.kong_enterprise_license.metadata.0.name
                key  = "license"
              }
            }
          }
          env {
            name  = "KONG_ADMIN_API_URI"
            value = var.kong_admin_host
          }
          env {
            name  = "KONG_ADMIN_GUI_AUTH"
            value = var.kong_admin_gui_auth
          }
          env {
            name  = "KONG_ENFORCE_RBAC"
            value = var.kong_enforce_rbac
          }
          env {
            name = "KONG_ADMIN_GUI_SESSION_CONF"
            value_from {
              config_map_key_ref {
                name = kubernetes_config_map.kong_session_conf.metadata.0.name
                key  = "admin-gui-session-conf"
              }
            }
          }
          env {
            name  = "KONG_DATABASE"
            value = local.kong_database
          }
          env {
            name  = "KONG_PG_HOST"
            value = var.kong_pg_host
          }
          env {
            name  = "KONG_PG_PASSWORD"
            value = var.kong_pg_password
          }
          env {
            name  = "KONG_NGINX_WORKER_PROCESSES"
            value = var.kong_nginx_worker_processes
          }
          env {
            name  = "KONG_NGINX_HTTP_INCLUDE"
            value = var.kong_nginx_http_include
          }
          env {
            name  = "KONG_ADMIN_ACCESS_LOG"
            value = var.kong_admin_access_log
          }
          env {
            name  = "KONG_ADMIN_ERROR_LOG"
            value = var.kong_admin_error_log
          }
          env {
            name  = "KONG_ADMIN_LISTEN"
            value = var.kong_admin_listen
          }
          env {
            name  = "KONG_PROXY_LISTEN"
            value = var.kong_proxy_listen
          }
          env {
            name  = "KONG_PORTAL"
            value = var.kong_portal
          }
          env {
            name  = "KONG_PORTAL_API_LISTEN"
            value = var.kong_portal_api_listen
          }
          env {
            name  = "KONG_PORTAL_GUI_LISTEN"
            value = var.kong_portal_gui_listen
          }
          env {
            name  = "KONG_PORTAL_GUI_HOST"
            value = var.kong_portal_host
          }
          env {
            name  = "KONG_PORTAL_GUI_PROTOCOL"
            value = var.kong_portal_gui_protocol
          }
          env {
            name  = "KONG_PORTAL_API_URL"
            value = local.kong_portal_api_url
          }
          env {
            name  = "KONG_PORTAL_API_ACCESS_LOG"
            value = var.kong_portal_api_access_log
          }
          env {
            name  = "KONG_PORTAL_API_ERROR_LOG"
            value = var.kong_portal_api_error_log
          }
          env {
            name  = "KONG_PORTAL_AUTH"
            value = var.kong_portal_auth
          }
          env {
            name = "KONG_PORTAL_SESSION_CONF"
            value_from {
              config_map_key_ref {
                name = kubernetes_config_map.kong_session_conf.metadata.0.name
                key  = "portal-session-conf"
              }
            }
          }
          lifecycle {
            pre_stop {
              exec {
                command = ["/bin/sh", "-c", "kong quit"]
              }
            }
          }
          liveness_probe {
            failure_threshold = 3
            http_get {
              path   = "/health"
              port   = 9001
              scheme = "HTTP"
            }
            initial_delay_seconds = 5
            period_seconds        = 10
            success_threshold     = 1
            timeout_seconds       = 1
          }
          readiness_probe {
            failure_threshold = 3
            http_get {
              path   = "/health"
              port   = 9001
              scheme = "HTTP"
            }
            initial_delay_seconds = 5
            period_seconds        = 10
            success_threshold     = 1
            timeout_seconds       = 1
          }
          security_context {
            run_as_user = 1000
          }
          volume_mount {
            mount_path = "/kong"
            name       = kubernetes_config_map.kong_server_blocks.metadata.0.name
          }
        }
        container {
          name    = "ingress-controller"
          image   = var.kong_ingress_image
          command = ["/kong-ingress-controller"]
          args = [
            "--admin-tls-skip-verify",
            "--admission-webhook-listen=0.0.0.0:8080",
            "--ingress-class=${var.ingress_class}",
            "--kong-url=https://localhost:8444",
            "--publish-service=${format("%s/%s", kubernetes_namespace.kong.metadata.0.name, kubernetes_service.kong_proxy.metadata.0.name)}",
          ]
          port {
            container_port = 8080
            name           = "webhook"
            protocol       = "TCP"
          }
          env {
            name = "CONTROLLER_KONG_ADMIN_TOKEN"
            value_from {
              secret_key_ref {
                name = kubernetes_secret.kong_enterprise_superuser_password.metadata.0.name
                key  = "password"
              }
            }
          }
          env {
            name = "POD_NAME"
            value_from {
              field_ref {
                api_version = "v1"
                field_path  = "metadata.name"
              }
            }
          }
          env {
            name = "POD_NAMESPACE"
            value_from {
              field_ref {
                api_version = "v1"
                field_path  = "metadata.namespace"
              }
            }
          }
          liveness_probe {
            failure_threshold = 3
            http_get {
              path   = "/healthz"
              port   = 10254
              scheme = "HTTP"
            }
            initial_delay_seconds = 5
            period_seconds        = 10
            success_threshold     = 1
            timeout_seconds       = 1
          }
          readiness_probe {
            failure_threshold = 3
            http_get {
              path   = "/healthz"
              port   = 10254
              scheme = "HTTP"
            }
            initial_delay_seconds = 5
            period_seconds        = 10
            success_threshold     = 1
            timeout_seconds       = 1
          }
          volume_mount {
            name       = "webhook-cert"
            mount_path = "/admission-webhook"
            read_only  = true
          }
        }
        volume {
          name = kubernetes_config_map.kong_server_blocks.metadata.0.name
          config_map {
            name = kubernetes_config_map.kong_server_blocks.metadata.0.name
          }
        }
        volume {
          name = "webhook-cert"
          secret {
            secret_name = kubernetes_secret.validation_webhook_keypair.metadata.0.name
          }
        }
      }
    }
  }
}

resource "kubernetes_deployment" "fallback" {
  metadata {
    name      = "fallback"
    namespace = kubernetes_namespace.kong.metadata.0.name
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        "app" = "fallback"
      }
    }
    template {
      metadata {
        labels = {
          "app" = "fallback"
        }
      }
      spec {
        container {
          name  = "fallback"
          image = "hashicorp/http-echo"
          args = [
            "-text",
            "This is not the path you are looking for. - Fallback service",
          ]
          port {
            container_port = 5678
            name           = "http"
            protocol       = "TCP"
          }
        }
      }
    }
  }
}

resource "kubernetes_stateful_set" "postgres" {
  metadata {
    name      = "postgres"
    namespace = kubernetes_namespace.kong.metadata.0.name
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        "app" = "postgres"
      }
    }
    service_name = "postgres"
    template {
      metadata {
        labels = {
          "app" = "postgres"
        }
      }
      spec {
        container {
          name  = "postgres"
          image = "postgres:9.5"
          port {
            container_port = 5432
            name           = "postgres"
            protocol       = "TCP"
          }
          env {
            name  = "POSTGRES_USER"
            value = var.kong_pg_user
          }
          env {
            name  = "POSTGRES_PASSWORD"
            value = var.kong_pg_password
          }
          env {
            name  = "POSTGRES_DB"
            value = var.kong_pg_database
          }
          env {
            name  = "PGDATA"
            value = "/var/lib/postgresql/data/pgdata"
          }
          volume_mount {
            mount_path = "/var/lib/postgresql/data"
            name       = "datadir"
            sub_path   = "pgdata"
          }
        }
        termination_grace_period_seconds = 60
      }
    }
    volume_claim_template {
      metadata {
        name = "datadir"
      }
      spec {
        access_modes = ["ReadWriteOnce"]
        resources {
          requests = {
            storage = "1Gi"
          }
        }
      }
    }
  }
}
resource "kubernetes_job" "kong_migrations" {
  metadata {
    name      = "kong-migrations"
    namespace = kubernetes_namespace.kong.metadata.0.name
  }
  spec {
    template {
      metadata {
        name = "kong-migrations"
      }
      spec {
        restart_policy = "OnFailure"
        image_pull_secrets {
          name = kubernetes_secret.kong_enterprise_edition_docker.metadata.0.name
        }
        init_container {
          name    = "wait-for-postgres"
          image   = "busybox"
          command = ["/bin/sh", "-c", "until nc -zv $(KONG_PG_HOST) $(KONG_PG_PORT) -w1; do echo 'waiting for db'; sleep 1; done"]
          env {
            name  = "KONG_PG_HOST"
            value = var.kong_pg_host
          }
          env {
            name  = "KONG_PG_PORT"
            value = var.kong_pg_port
          }
        }
        container {
          name    = "kong-migrations"
          image   = var.kong_image
          command = ["/bin/sh", "-c", "kong migrations bootstrap"]
          env {
            name = "KONG_LICENSE_DATA"
            value_from {
              secret_key_ref {
                name = kubernetes_secret.kong_enterprise_license.metadata.0.name
                key  = "license"
              }
            }
          }
          env {
            name = "KONG_PASSWORD"
            value_from {
              secret_key_ref {
                name = kubernetes_secret.kong_enterprise_superuser_password.metadata.0.name
                key  = "password"
              }
            }
          }
          env {
            name  = "KONG_PG_HOST"
            value = var.kong_pg_host
          }
          env {
            name  = "KONG_PG_PASSWORD"
            value = var.kong_pg_password
          }
          env {
            name  = "KONG_PG_PORT"
            value = var.kong_pg_port
          }
        }
        volume {
          name = "webhook-cert"
          secret {
            secret_name = kubernetes_secret.validation_webhook_keypair.metadata.0.name
          }
        }
      }
    }
  }
}
